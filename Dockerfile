FROM francarmona/docker-ubuntu16-nginx-php7
LABEL maintainer="Lars Lehmann <ll@stadtlandnetz.de>"

USER root

RUN apt update && apt-get install -y libxrender1 fonts-lmodern fonts-lato fonts-freefont-ttf fonts-dejavu-extra fonts-dejavu-core fonts-dejavu fontconfig-config fontconfig libflac8 libfontenc1 libfreetype6 
RUN apt install -y xfonts-75dpi xfonts-base xfonts-encodings xfonts-utils gsfonts ttf-freefont texlive-font-utils libxfont1 libfont-afm-perl pdftk

COPY --chown=root wkhtmltopdf /usr/bin/

RUN rm -R /usr/share/fonts
COPY --chown=root fonts /usr/share/fonts
RUN chmod 755 /usr/bin/wkhtmltopdf

COPY files /var/www/html/

COPY ./init.sh /init.sh
CMD ["/init.sh"]
