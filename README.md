**VIA PDFPrint-Server**

**eine kleine PHP Anwendung, welche sich um den PDF-Druck in VIA kümmert**

- PDFs aus HTML erstellen
- mehrere PDFs zusammenfügen
- PDFs zum Druck & Download anbieten

**folgende Tools müssen installiert werden**

- php server > 5.0
- wkhtmltopdf
- pdftk

**Troubleshooting**

- fehlerhafte Anzeige (keine Bilder, falsche Fonts..): Installation von wkhtmltopdf prüfen, da fehlerhaftes Package installiert ist