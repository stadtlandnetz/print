#!/usr/bin/env sh

# We change the ownership here at runtime and not on build time because
# /var/www/html is marked as a volume in the base image. That's why Docker
# will automatically give this directory to the user declared in the Dockerfile
# which is root in our case.
# Unfortunately, we cannot simply change the USER directive in the Dockerfile
# because supervisord seems to need root privileges.
# Not a nice solution => but it works...
chmod -R 777 /var/www/html/
chown -R www-data:www-data /var/www/html

/usr/bin/supervisord