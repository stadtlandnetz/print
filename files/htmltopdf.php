<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
$data = (object)$_POST;
$filename = $data->filename;
$html = file_get_contents("dok_dummy.html");

if (!file_exists($data->instance)) {
    mkdir($data->instance, 0777, true);
}
if (!file_exists($data->instance."_temp")) {
    mkdir($data->instance."_temp", 0777, true);
}

$html = str_replace("{{baseurl}}",$data->baseurl,$html);
$html = str_replace("{{CSS}}",$data->custom_css,$html);

file_put_contents($filename. ".html", $html);

$bottom = floatval($data->bottom);
$top = floatval($data->top);
$left = floatval($data->left);
$right = floatval($data->right);

$landscape = "";
if ($data->landscape == 1) {
	$landscape = "-O landscape";
}
if ($data->print_footer == 1 || $data->print_footer == 2) {
file_put_contents($filename. "_footer.html", "<html>  
<head>  
<script>  
   function subst() {  
      var vars = {};  
        
      // explode the URL query string  
      var x = document.location.search.substring(1).split('&');  
        
      // loop through each query string segment and get keys/values  
      for (var i in x)  
      {  
         var z = x[i].split('=',2);  
         vars[z[0]] = unescape(z[1]);  
      }  
  
      // an array of all the parameters passed into the footer file  
      var x = ['frompage', 'topage', 'page', 'webpage', 'section', 'subsection', 'subsubsection'];  
        
      // each page will have an element with class 'section' from the body of the footer HTML  
      var y = document.getElementsByClassName('section');  
      for(var j = 0; j < y.length; j++)  
      {  
      	var check = ('".($data->print_footer == 1)."' === '1') ? 1 : vars[x[1]];
      	var content = ".json_encode($data->footer_html).";
      	
      	      	// if current page equals total pages  
         if (vars[x[2]] == check)  
         {  
            y[j].innerHTML = '<!DOCTYPE html><div class=\'dok-footer\'>'+content+'</div>';  
         }  
      }  
   }  
</script>  
</head>  
<body onload='subst()'>  
   <div class='section'></div>  
</body>  
</html> ");
} elseif ($data->print_footer == 3) {

file_put_contents($filename. "_footer.html", "<html><head><meta charset='utf-8'><script>
function subst() {
  var vars={};
  var x=document.location.search.substring(1).split('&');
  for(var i in x) {var z=x[i].split('=',2);vars[z[0]] = decodeURIComponent(z[1])};
  var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];
  for(var i in x) {
    var y = document.getElementsByClassName(x[i]);
    for(var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];
  }
}
</script></head><body style='border:0; margin: 0;' onload='subst()'>
<table style='border-bottom: 1px solid black; width: 100%'>
  <tr>
    <td class='section'></td>
    <td style='text-align:right'>
      Seite <span class='page'></span> von <span class='topage'></span>
    </td>
  </tr>
</table>
</body></html>");
} else {
file_put_contents($filename. "_footer.html", "<!DOCTYPE html><div class='dok-footer'>".$data->footer_html."</div>");
}

if ($data->print_header == 2) {
	file_put_contents($filename. "_header.html", "<!DOCTYPE html><div class='dok-header'>".$data->header_html."</div>");
	$html = str_replace("{{body}}","<div class='dok-body'>".$data->html."</div>",$html);
} else {
	file_put_contents($filename. "_header.html","<!DOCTYPE html>");
	$html = str_replace("{{body}}","<div class='dok-header'>".$data->header_html . "</div><div class='dok-body'>" .$data->html. "</div>",$html);
}

// smartshrink, better turn off
$smartshrink = "";
if (strpos($filename,"VVV") !== false or strpos($filename,"vvv") !== false) {
	$smartshrink = "--dpi 380 --disable-smart-shrinking";	
}
if (strpos($filename,"LKLA") !== false or strpos($filename,"lkla") !== false) {
	$smartshrink = "--dpi 72 --disable-smart-shrinking";	
}
if (strpos($filename,"LA") !== false or strpos($filename,"la") !== false) {
	$smartshrink = "--dpi 72 --disable-smart-shrinking";	
}
// if (strpos($filename,"VMS") !== false) {
// 	$smartshrink = "--dpi 380 --disable-smart-shrinking";	
// }
if (strpos($filename,"LKKA") !== false or strpos($filename,"lkka") !== false) {
	$smartshrink = "--dpi 380 --disable-smart-shrinking";	
}
if (strpos($filename,"FRANZISKUS") !== false or strpos($filename,"franziskus") !== false) {
	$smartshrink = "--dpi 380 --disable-smart-shrinking";	
}
if (strpos($filename,"GTH") !== false or strpos($filename,"gth") !== false) {
	$smartshrink = "--dpi 380 --disable-smart-shrinking";	
}


file_put_contents($filename. ".html", $html);
shell_exec("wkhtmltopdf --header-html '".$filename."_header.html' --footer-html '".$filename."_footer.html' -T ".$top."mm -B ".$bottom."mm -R ".$right."mm -L ".$left."mm ".$landscape." ".$smartshrink." '$filename.html' '".$data->instance."_temp/".$filename.".pdf'");
//
shell_exec("mv '".$data->instance."_temp/".$filename.".pdf' '".$data->instance."/".$filename.".pdf'");
shell_exec("rm ".$filename.".html"); // diese Zeile auskommentieren, dann werden die html files nicht gelöscht und man kann debuggen. die Datei ist dann über "https://print.via-cloud.de/filename_der_pdf.pdf" erreichbar.
shell_exec("rm ".$filename."_header.html");
shell_exec("rm ".$filename."_footer.html");

include('delete_old_files.php');

?>
