<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
$di = new RecursiveDirectoryIterator('/var/www/html');
foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
	if (pathinfo($file, PATHINFO_EXTENSION) == "pdf") {
	 	if((time() - filemtime($file)) > (15 * 60) ) {
	 		unlink($filename);
	 	}
	 }
}

?>