<?php 
    $data = (object)$_POST;

    if ($data->action == "start_job") {
        if (!file_exists($data->action_id)) {
            mkdir($data->action_id, 0777, true);
        }
        if (!file_exists($data->action_id."_temp")) {
            mkdir($data->action_id."_temp", 0777, true);
        }
    } elseif ($data->action == "add_to_job") {
        $filename = $data->filename;
                                              
        $html = file_get_contents("dok_dummy.html");
        
        
		$html = str_replace("{{baseurl}}",$data->baseurl,$html);
		$html = str_replace("{{CSS}}",$data->custom_css,$html);

        file_put_contents($filename. ".html", $html);
                                              
         $bottom = floatval($data->bottom);
         $top = floatval($data->top);
         $left = floatval($data->left);
         $right = floatval($data->right);
                                              
         $landscape = "";
         if ($data->landscape == 1) {
            $landscape = "-O landscape";
         }

         if ($data->print_footer == 1 || $data->print_footer == 2) {
            file_put_contents($filename. "_footer.html", "<html>  
            <head>  
            <script>  
               function subst() {  
                  var vars = {};  
                    
                  // explode the URL query string  
                  var x = document.location.search.substring(1).split('&');  
                    
                  // loop through each query string segment and get keys/values  
                  for (var i in x)  
                  {  
                     var z = x[i].split('=',2);  
                     vars[z[0]] = unescape(z[1]);  
                  }  
              
                  // an array of all the parameters passed into the footer file  
                  var x = ['frompage', 'topage', 'page', 'webpage', 'section', 'subsection', 'subsubsection'];  
                    
                  // each page will have an element with class 'section' from the body of the footer HTML  
                  var y = document.getElementsByClassName('section');  
                  for(var j = 0; j < y.length; j++)  
                  {  
                      var check = ('".($data->print_footer == 1)."' === '1') ? 1 : vars[x[1]];
                      var content = ".json_encode($data->footer_html).";
                      
                                // if current page equals total pages  
                     if (vars[x[2]] == check)  
                     {  
                        y[j].innerHTML = '<!DOCTYPE html><div class=\'dok-footer\'>'+content+'</div>';  
                     }  
                  }  
               }  
            </script>  
            </head>  
            <body onload='subst()'>  
               <div class='section'></div>  
            </body>  
            </html> ");
            } else {
            file_put_contents($filename. "_footer.html", "<!DOCTYPE html><div class='dok-footer'>".$data->footer_html."</div>");
            }
            
            if ($data->print_header == 2) {
                file_put_contents($filename. "_header.html", "<!DOCTYPE html><div class='dok-header'>".$data->header_html."</div>");
                $html = str_replace("{{body}}","<div class='dok-body'>".$data->html."</div>",$html);
            } else {
                file_put_contents($filename. "_header.html","<!DOCTYPE html>");
                $html = str_replace("{{body}}","<div class='dok-header'>".$data->header_html . "</div><div class='dok-body'>" .$data->html. "</div>",$html);
            }

            file_put_contents($filename. ".html", $html);
            shell_exec("wkhtmltopdf --header-html '".$filename."_header.html' --footer-html '".$filename."_footer.html' -T ".$top."mm -B ".$bottom."mm -R ".$right."mm -L ".$left."mm ".$landscape." ".$smartshrink." '$filename.html' '".$data->action_id."_temp/".$filename.".pdf'");
            //
            shell_exec("mv '".$data->action_id."_temp/".$filename.".pdf' '".$data->action_id."/".$filename.".pdf'");
            shell_exec("rm ".$filename.".html");
            shell_exec("rm ".$filename."_header.html");
            shell_exec("rm ".$filename."_footer.html");
    } elseif ($data->action == "finish_job") {
            shell_exec("zip -r ".$data->action_id.".zip ".$data->action_id);
            shell_exec("rm -r ".$data->action_id."/");
            shell_exec("rm -r ".$data->action_id."_temp/");
    } elseif ($data->action == "delete_job") {
        shell_exec("rm ".$data->action_id.".zip");
    } elseif ($_GET["action"] == "get_progress") {
                              $directory = $_GET["action_id"];
                              $filecount = 0;
                              $files = glob($directory . "*");
                              if ($files){
                              $filecount = count($files);
                              }
                              echo $filecount ;
    }

?>
