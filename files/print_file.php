<script   src="https://code.jquery.com/jquery-3.3.1.slim.min.js"   integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="   crossorigin="anonymous"></script>
<script>
 $(function() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');
    var edge = ua.indexOf('Edge/');
    var url = '<?php echo $_GET["pdf"]?>';
    var wanted = '<?php echo !empty($_GET["wanted"]) ?  $_GET["wanted"] : $_GET["pdf"]; ?>';
    var pdf ='';
    var style = 'position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden;';

    if(msie > 0 || trident > 0 || edge > 0){
        pdf = '<object data="https://print.via-cloud.de/serve_file.php?origin=' + url + '&wanted='+wanted+'" name="print_frame" id="print_frame" style="' + style + '" type="application/pdf">';
    }
    else{
        pdf ='<iframe src="https://print.via-cloud.de/serve_file.php?origin=' + url + '&wanted='+wanted+'" name="print_frame" id="print_frame" style="' + style + '"></iframe>';
    }

    $(document.body).append(pdf);
});
</script>
<html>
<title><?php echo !empty($_GET["wanted"]) ?  $_GET["wanted"] : $_GET["pdf"];?></title>
</html>
